import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import {Rygg} from './Components/Rygg';
import {Armer} from './Components/Armer';
import {Bryst} from './Components/Bryst';
import {Bein} from './Components/Bein';
import {Kosthold} from './Components/Kosthold';
import {Header} from './Components/Header';
import {Homepage} from './Components/Homepage';
import {BrowserRouter as Router, Route} from 'react-router-dom';



class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Header />
            <Route exact path="/" component={Homepage}/>
            <Route path="/rygg" component={Rygg}/>
            <Route path="/armer" component={Armer}/>
            <Route path="/bein" component={Bein}/>
            <Route path="/bryst" component={Bryst}/>
            <Route path="/kosthold" component={Kosthold}/>
        </div>
      </Router>
    )
  };
}

export default App;
