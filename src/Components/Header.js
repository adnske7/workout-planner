import React, { Component } from 'react'
import {Nav, Form, Navbar, FormControl, Button, NavDropdown} from 'react-bootstrap';

export class Header extends Component {
    render() {
        return (
            <Navbar bg="light" expand="lg">
  <Navbar.Brand href="/">
  <img
        src="/dumbbell.png"
        width="30"
        height="30"
        className="d-inline-block align-top"
        alt="React Bootstrap logo"
      />
  </Navbar.Brand>
  <Navbar.Toggle aria-controls="basic-navbar-nav" />
  <Navbar.Collapse id="basic-navbar-nav">
    <Nav className="mr-auto">
      <NavDropdown title="Meny" id="basic-nav-dropdown">
        <NavDropdown.Item href="/">Homepage</NavDropdown.Item>
        <NavDropdown.Item href="/rygg">Rygg</NavDropdown.Item>
        <NavDropdown.Item href="/bryst">Bryst</NavDropdown.Item>
        <NavDropdown.Item href="/bein">Bein</NavDropdown.Item>
        <NavDropdown.Item href="/armer">Armer</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="/kosthold">Kosthold</NavDropdown.Item>
      </NavDropdown>
    </Nav>
    <Form inline>
      <FormControl type="text" placeholder="Søk" className="mr-sm-2" />
      <Button variant="outline-success">Søk</Button>
    </Form>
  </Navbar.Collapse>
</Navbar>
        )
    }
}

export default Header;